﻿using System;
using LibCodeUsageFinder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LibCodeUsageFinderTests
{
    [TestClass]
    public class CodeUsageFinderTests
    {
        [TestMethod]
        public void IntegrationTest()
        {
            Usages usages = new CodeUsageFinder()
                .InDirectoryRecursively("..\\..\\TestTypes")
                .FindTypeUsage("LibCodeUsageFinderTests.TestTypes.NamespaceA.UsedClassA");

            usages.ToList()
                .ForEach(usage => Assert.IsTrue(
                    usage.Line.Content.Contains("// [Matching identifier]"),
                    "False positive usage: \"{0}\" does not use specified type",
                    usage
                ));
        }
    }
}
