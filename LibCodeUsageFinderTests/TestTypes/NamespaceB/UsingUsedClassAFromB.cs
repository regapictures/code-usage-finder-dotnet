﻿using LibCodeUsageFinderTests.TestTypes.NamespaceA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibCodeUsageFinderTests.TestTypes.NamespaceB
{
    public class UsingUsedClassAFromB // [No matching identifier]
    {
        private UsedClassA _privateMember; // [Matching identifier]

        public UsingUsedClassAFromB() // [No matching identifier]
        {
            _privateMember = new UsedClassA(); // [Matching identifier]
        }

        public UsingUsedClassAFromB // [No matching identifier]
            (UsedClassA providedValueForPrivateMember) // [Matching identifier]
        {
            _privateMember = providedValueForPrivateMember;
        }

        public UsedClassA PublicAutoProperty { get; set; } // [Matching identifier]
        public UsedClassA PublicPropertyReferencingPrivateMember => _privateMember; // [Matching identifier]

        public void UseInline()
        {
            new UsedClassA().Hello("Bob"); // [Matching identifier]
        }

        public void UsePrivateMember()
        {
            _privateMember.Hello("Bob");
        }
    }
}
