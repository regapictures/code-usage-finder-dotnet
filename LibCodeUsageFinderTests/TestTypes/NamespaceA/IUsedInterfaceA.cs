﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibCodeUsageFinderTests.TestTypes.NamespaceA
{
    public interface IUsedInterfaceA
    {
        void Hello(string name);
    }
}
