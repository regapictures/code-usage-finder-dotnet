﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibCodeUsageFinderTests.TestTypes.NamespaceA
{
    public class UsedClassA: UsedBaseClassA // [Matching identifier]
    {
        public override void Hello(string name)
        {
            Console.WriteLine("Hello {0}!", name);
        }
    }
}
