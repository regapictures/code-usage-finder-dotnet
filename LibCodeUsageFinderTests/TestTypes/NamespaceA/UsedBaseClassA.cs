﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibCodeUsageFinderTests.TestTypes.NamespaceA
{
    public abstract class UsedBaseClassA : IUsedInterfaceA
    {
        public abstract void Hello(string name);
    }
}
