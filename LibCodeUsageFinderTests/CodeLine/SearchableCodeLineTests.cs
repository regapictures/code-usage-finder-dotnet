﻿using LibCodeUsageFinder;
using LibCodeUsageFinder.CodeLine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace LibCodeUsageFinderTests.CodeLine
{
    [TestClass]
    public class SearchableCodeLineTests
    {
        [TestMethod]
        public void FindUsingDirectives_WhenUsingDirectiveHasOneIdentifier_ShouldReturnCorrectMatch()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "using System;");

            List<string> usingDirectives = line.FindUsingDirectives();
            Assert.AreEqual(1, usingDirectives.Count);
            Assert.AreEqual("System", usingDirectives[0]);
        }

        [TestMethod]
        public void FindUsingDirectives_WhenUsingDirectiveHasOneIdentifierWIthWhiteSpace_ShouldReturnCorrectMatch()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, " \t \tusing  \t  System \t\t ;  \t \n");

            List<string> usingDirectives = line.FindUsingDirectives();
            Assert.AreEqual(1, usingDirectives.Count);
            Assert.AreEqual("System", usingDirectives[0]);
        }

        [TestMethod]
        public void FindUsingDirectives_WhenUsingDirectiveHasDotNotation_ShouldReturnCorrectMatch()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "using Microsoft.VisualStudio.TestTools.UnitTesting;");

            List<string> usingDirectives = line.FindUsingDirectives();
            Assert.AreEqual(1, usingDirectives.Count);
            Assert.AreEqual("Microsoft.VisualStudio.TestTools.UnitTesting", usingDirectives[0]);
        }

        [TestMethod]
        public void FindUsingDirectives_WhenUsingDirectiveHasDotNotationWithWhiteSpace_ShouldReturnCorrectMatch()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, " \t \tusing  \t  Microsoft.VisualStudio.TestTools.UnitTesting \t\t ;  \t \n");

            List<string> usingDirectives = line.FindUsingDirectives();
            Assert.AreEqual(1, usingDirectives.Count);
            Assert.AreEqual("Microsoft.VisualStudio.TestTools.UnitTesting", usingDirectives[0]);
        }

        [TestMethod]
        public void FindUsingDirectives_WhenMultipleUsingDIrectivesOnOneLine_ShouldReturnMultipleMatches()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "using System.Collections.Generic; using Microsoft.VisualStudio.TestTools.UnitTesting;");

            List<string> usingDirectives = line.FindUsingDirectives();
            Assert.AreEqual(2, usingDirectives.Count);
            Assert.AreEqual("System.Collections.Generic", usingDirectives[0]);
            Assert.AreEqual("Microsoft.VisualStudio.TestTools.UnitTesting", usingDirectives[1]);
        }

        [TestMethod]
        public void FindUsingDirectives_WhenUsingAtCharacterInIdentifiers_ShouldReturnCorrectMatch()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "using @string.@int.@if;");

            List<string> usingDirectives = line.FindUsingDirectives();
            Assert.AreEqual(1, usingDirectives.Count);
            Assert.AreEqual("@string.@int.@if", usingDirectives[0]);
        }

        [TestMethod]
        public void FindUsingDirectives_WhenUsingDoubleDots_ShouldNotReturnMatches()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "using Microsoft.VisualStudio..TestTools.UnitTesting;");

            List<string> usingDirectives = line.FindUsingDirectives();
            Assert.AreEqual(0, usingDirectives.Count);
        }

        [TestMethod]
        public void FindText_WhenLineContainsLowerCaseVariableNameEqualToTypeName_ShouldNotReturnMatches()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "var pen = new SomeBrush();");

            Usages usages = line.FindText("Pen");
            Assert.AreEqual(0, usages.Count);
            Assert.IsFalse(usages.Any());
        }

        [TestMethod]
        public void FindText_WhenLineContainsSearchTextOnce_ShouldReturnOneMatch()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "new Pen();");

            Usages usages = line.FindText("Pen");
            Assert.AreEqual(1, usages.Count);
            Assert.IsTrue(usages.Any());
        }

        [TestMethod]
        public void FindText_WhenLineContainsSearchTextTwice_ShouldReturnTwoMatches()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "Pen pen = new Pen();");

            Usages usages = line.FindText("Pen");
            Assert.AreEqual(2, usages.Count);
            Assert.IsTrue(usages.Any());
        }

        [TestMethod]
        public void FindText_WhenLineContainsNonMatchingClassContainingIdentifier_ShouldReturnMatches()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "MyPension pension = new MyPension();");

            Usages usages = line.FindText("Pen");
            Assert.AreEqual(2, usages.Count);
            Assert.IsTrue(usages.Any());
        }

        [TestMethod]
        public void FindIdentifier_WhenLineContainsMatchingIdentifier_ShouldReturnMatches()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "Pen pen = new Pen();");

            Usages usages = line.FindIdentifier("Pen");
            Assert.AreEqual(2, usages.Count);
            Assert.IsTrue(usages.Any());
        }

        [TestMethod]
        public void FindIdentifier_WhenLineContainsOneMatchingIdentifierAndOneNonMatching_ShouldReturnOneMatch()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "Pen pen = new SpecialPen();");

            Usages usages = line.FindIdentifier("Pen");
            Assert.AreEqual(1, usages.Count);
            Assert.AreEqual(0, usages.ToList()[0].CharacterRange.StartIndex);
        }

        [TestMethod]
        public void FindIdentifier_WhenLineContainsNonMatchingClassContainingIdentifier_ShouldNotReturnMatches()
        {
            SearchableCodeLine line = SearchableCodeLine.From("", 0, "MyPension pension = new MyPension();");

            Usages usages = line.FindIdentifier("Pen");
            Assert.AreEqual(0, usages.Count);
            Assert.IsFalse(usages.Any());
        }
    }
}
