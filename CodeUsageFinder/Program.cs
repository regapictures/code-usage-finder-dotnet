﻿using LibCodeUsageFinder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CodeUsageFinderConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().Run(args);
            Console.ReadKey();
        }

        public void Run(string[] args)
        {
            List<string> lowerCaseArgs = args.Select(arg => arg.ToLower()).ToList();
            int typeBooleanIndex = lowerCaseArgs.IndexOf("--type");

            if (args.Length < 3)
            {
                Console.WriteLine("Not enough arguments.");
                DisplayUsage();
            }
            else if (typeBooleanIndex != -1 && typeBooleanIndex + 2 < args.Length)
            {
                Usages usages = FindUsages(args, typeBooleanIndex);
                Print(usages);
            }
            else
            {
                DisplayUsage();
            }
        }

        private Usages FindUsages(string[] args, int typeBooleanIndex)
        {
            try
            {
                return FindUsagesUnsafe(args, typeBooleanIndex);
            }
            catch (DirectoryNotFoundException e)
            {
                Console.Error.WriteLine("Directory not found: {0}", e.Data["path"]);
                return Usages.Empty();
            }
        }

        private Usages FindUsagesUnsafe(string[] args, int typeBooleanIndex)
        {
            return new CodeUsageFinder()
                    .InDirectoryRecursively(args[args.Length - 1])
                    .FindTypeUsage(args[typeBooleanIndex + 1]);
        }

        private void Print(Usages usages)
        {
            usages.ToList()
                .ForEach(usage => Console.WriteLine("{0}", usage));
        }

        private void DisplayUsage()
        {
            Console.WriteLine("Usage: {0} --type <full type identifier> root-search-folder", getAssemblyName());
        }

        private string getAssemblyName()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            return Path.GetFileName(codeBase);
        }
    }
}
