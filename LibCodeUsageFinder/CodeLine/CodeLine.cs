﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LibCodeUsageFinder.CodeLine
{
    public class CodeLine
    {
        public CodeLine(string filePath, int lineIndex, string content)
        {
            FilePath = filePath;
            LineIndex = lineIndex;
            Content = content;
        }

        public string FilePath { get; }
        public int LineIndex { get; }
        public string Content { get; }
    }
}
