﻿using LibCodeUsageFinder.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LibCodeUsageFinder.CodeLine
{
    public abstract class SearchableCodeLine : CodeLine
    {
        protected SearchableCodeLine(string filePath, int lineIndex, string content) : base(filePath, lineIndex, content)
        {
        }

        public static SearchableCodeLine From(string filePath, int lineIndex, string content)
        {
            return string.IsNullOrWhiteSpace(content) ? (SearchableCodeLine)new EmptyLine(filePath, lineIndex) : new NonEmptyLine(filePath, lineIndex, content);
        }

        public abstract Usages FindText(string text);
        public abstract Usages FindIdentifier(string identifier);
        public abstract List<string> FindUsingDirectives();

        private class Regexes
        {
            public const string WhiteSpace = "\\s+";
            public const string OptionalWhiteSpace = "\\s*";
            public const string IdentifierPattern = "@?[A-Za-z_]\\w*";

            public static readonly Regex UsingDirective = new Regex(
                "using" +
                WhiteSpace +
                "(" + IdentifierPattern + "(\\." + IdentifierPattern + ")*)" +
                OptionalWhiteSpace +
                ";");
            public const int UsingDirective_Value_GroupIndex = 1;

            public static readonly Regex Identifier = new Regex(IdentifierPattern);
        }

        private class EmptyLine : SearchableCodeLine
        {
            public EmptyLine(string filePath, int lineIndex) : base(filePath, lineIndex, "")
            {
            }

            public override Usages FindText(string text)
            {
                return Usages.Empty();
            }

            public override Usages FindIdentifier(string identifier)
            {
                return Usages.Empty();
            }

            public override List<string> FindUsingDirectives()
            {
                return new List<string>();
            }
        }

        private class NonEmptyLine : SearchableCodeLine
        {
            public NonEmptyLine(string filePath, int lineIndex, string content) : base(filePath, lineIndex, content)
            {
            }

            public override Usages FindText(string text)
            {
                if (string.IsNullOrEmpty(text))
                    return Usages.Empty();

                int textLength = text.Length;
                return Content.AllIndexesOf(text)
                    .Select(i => new Usage(FilePath, this, new CharacterRange(i, textLength)))
                    .ToUsages();
            }

            public override Usages FindIdentifier(string identifier)
            {
                if (string.IsNullOrEmpty(identifier))
                    return Usages.Empty();

                int identifierLength = identifier.Length;
                return Content.AllIndexesOf(identifier)
                    .Where(i => i == 0 || i == Content.Length - 1
                        || IsIdentifier(Content, identifier, identifierLength, i))
                    .Select(i => new Usage(FilePath, this, new CharacterRange(i, identifierLength)))
                    .ToUsages();
            }

            private bool IsIdentifier(string content, string identifier, int identifierLength, int i)
            {
                Match match = Regexes.Identifier.Match(Content.Substring(i - 1, identifierLength + 1));
                return match.Success ? match.Index > 0 && match.Length == identifierLength : false;
            }

            public override List<string> FindUsingDirectives()
            {
                return Regexes.UsingDirective.Matches(Content)
                    .Cast<Match>()
                    .Select(match => match.Groups[Regexes.UsingDirective_Value_GroupIndex].Value)
                    .ToList();
            }
        }
    }
}
