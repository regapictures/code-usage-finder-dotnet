﻿using LibCodeUsageFinder.CodeLine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibCodeUsageFinder.UsageFinders.File
{
    class TextualSearchFinder : AbstractBaseInFileFinder
    {
        public TextualSearchFinder(string filePath) : base(filePath)
        {
        }

        public override Usages FindTypeUsage(string fullName)
        {
            string typeName = fullName.Split('.').Last();
            UsagesBuilder builder = new UsagesBuilder();

            int lineIndex = 0;
            using (StreamReader reader = new StreamReader(_filePath))
            {
                while (reader.Peek() >= 0)
                {
                    string line = reader.ReadLine();

                    Usages usages = SearchableCodeLine.From(_filePath, lineIndex, line).FindIdentifier(typeName);
                    if (usages.Any())
                        builder.AddRange(usages);

                    lineIndex++;
                }
            }

            return builder.Build();
        }
    }
}
