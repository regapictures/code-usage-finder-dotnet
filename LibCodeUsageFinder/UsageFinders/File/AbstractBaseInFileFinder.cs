﻿using System.IO;

namespace LibCodeUsageFinder.UsageFinders.File
{
    public abstract class AbstractBaseInFileFinder : IUsagesFinder
    {
        protected string _filePath;

        public AbstractBaseInFileFinder(string filePath)
        {
            _filePath = filePath;
        }

        public abstract Usages FindTypeUsage(string fullName);
    }
}
