﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibCodeUsageFinder.UsageFinders
{
    public interface IUsagesFinder
    {
        Usages FindTypeUsage(string fullName);
    }
}
