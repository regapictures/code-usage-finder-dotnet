﻿using System.Collections.Generic;
using System.Linq;

namespace LibCodeUsageFinder.UsageFinders
{
    public class UsagesFinders : IUsagesFinder
    {
        private List<IUsagesFinder> _inFileFinders;

        public UsagesFinders(IUsagesFinder inFileFinder, params IUsagesFinder[] inFileFinders)
        {
            _inFileFinders = new List<IUsagesFinder>();
            _inFileFinders.Add(inFileFinder);
            _inFileFinders.AddRange(inFileFinders);
        }

        public UsagesFinders(List<IUsagesFinder> inFileFinders)
        {
            _inFileFinders = inFileFinders;
        }

        public Usages FindTypeUsage(string fullName)
        {
            return _inFileFinders
                .Select(finder => finder.FindTypeUsage(fullName))
                .Aggregate(Usages.Empty(), (Usages usages1, Usages usages2) => usages1.Merge(usages2));
        }
    }
}
