﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibCodeUsageFinder.CodeLine;

namespace LibCodeUsageFinder
{
    public class Usage
    {
        public Usage(string path, CodeLine.CodeLine line, CharacterRange characterRange)
        {
            Path = path;
            Line = line;
            CharacterRange = characterRange;
        }

        public string Path { get; }
        public string FileName => System.IO.Path.GetFileName(Path);
        public CodeLine.CodeLine Line { get; }
        public CharacterRange CharacterRange { get; }

        public override string ToString()
        {
            return Path + ":" + (Line.LineIndex + 1) + ", " + CharacterRange.StartIndex + " " + Line.Content.Trim();
        }
    }

    public class CharacterRange
    {
        public CharacterRange(int startIndex, int length)
        {
            StartIndex = startIndex;
            Length = length;
        }

        public int StartIndex { get; }
        public int Length { get; }
    }
}
