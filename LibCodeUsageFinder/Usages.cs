﻿using System.Collections.Generic;
using System.Linq;

namespace LibCodeUsageFinder
{
    public class Usages
    {
        private List<Usage> _usages;

        public Usages(List<Usage> usages)
        {
            _usages = new List<Usage>(usages);
        }

        public static Usages Empty()
        {
            return new Usages(new List<Usage>());
        }

        public int Count => _usages.Count;

        public bool Any()
        {
            return _usages.Any();
        }

        public Usages Merge(Usages usages)
        {
            var ugs = new List<Usage>(_usages);
            ugs.AddRange(usages._usages);
            return new Usages(ugs);
        }

        public List<Usage> ToList()
        {
            return new List<Usage>(_usages);
        }
    }

    public static class UsagesExtensions
    {
        public static Usages ToUsages(this IEnumerable<Usage> iEnumerable)
        {
            return new Usages(iEnumerable.ToList());
        }
    }
}
