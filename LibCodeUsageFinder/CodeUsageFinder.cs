﻿using LibCodeUsageFinder.UsageFinders;
using LibCodeUsageFinder.UsageFinders.File;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LibCodeUsageFinder
{
    public class CodeUsageFinder
    {
        public IUsagesFinder InDirectoryRecursively(string path)
        {
            MustBeExistingDirectory(path);
            return new UsagesFinders(
                InDirectory(path),
                Directory.GetDirectories(path, "*.*", SearchOption.AllDirectories)
                    .Select(pth => InDirectory(pth))
                    .ToArray()
            );
        }

        public IUsagesFinder InDirectory(string path)
        {
            MustBeExistingDirectory(path);
            return InFiles(Directory.GetFiles(path));
        }

        public IUsagesFinder InFiles(IEnumerable<string> paths)
        {
            return new UsagesFinders(
                paths
                    .Where(path => path.EndsWith(".cs")) // Todo Determine if this needs to be configurable?
                    .Select(path => InFile(path))
                    .ToList()
            );
        }

        public IUsagesFinder InFile(string path)
        {
            MustBeExistingFile(path);
            return new TextualSearchFinder(path);
        }

        private void MustBeExistingDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path + " is not a(n existing) directory")
                {
                    Data = { { "path", path } }
                };
            }
        }

        private void MustBeExistingFile(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException(path + " is not a(n existing) file")
                {
                    Data = { { "path", path } }
                };
            }
        }
    }
}
