﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibCodeUsageFinder
{
    public class UsagesBuilder
    {
        private List<Usage> _usages = new List<Usage>();

        public UsagesBuilder AddRange(Usages usages)
        {
            _usages.AddRange(usages.ToList());
            return this;
        }

        public Usages Build()
        {
            return new Usages(_usages);
        }
    }
}
